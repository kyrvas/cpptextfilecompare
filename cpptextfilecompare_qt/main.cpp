#include "textfilecompare.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TextFileCompare w;
    w.show();
    return a.exec();
}
