#ifndef TEXTFILECOMPARE_H
#define TEXTFILECOMPARE_H

#include <QMainWindow>

#include <QTimer>
#include <QTime>
#include <QFuture>

QT_BEGIN_NAMESPACE
namespace Ui { class TextFileCompare; }
QT_END_NAMESPACE

class TextFileCompare : public QMainWindow
{
    Q_OBJECT

public:
    TextFileCompare(QWidget *parent = nullptr);
    ~TextFileCompare();

private slots:
    void on_selectFile1Button_clicked();

    void on_selectFile2Button_clicked();

    void on_compareButton_clicked();

    void updateTime();
    void on_radioButton1_toggled(bool checked);

private:
    Ui::TextFileCompare *ui;
    void selectFilename(std::string &fname);
    void setupTimer();
    void enableControls();
    void disableControls();

    std::string file1_name;
    std::string file2_name;
    const std::string comparedFilename;
    const int TIMER_TICK;
    QFuture<bool> future_result;

    bool compareToFirstFile;
    QTimer * timer;
};
#endif // TEXTFILECOMPARE_H
