#include "textfilecompare.h"
#include "./ui_textfilecompare.h"
#include <QFileDialog>

#include "Renderer.h"
#include "Comparer.h"
#include "ColorText.h"
#include "ColorLine.h"
#include "TextFile.h"

#include <QtConcurrent/QtConcurrentRun>

TextFileCompare::TextFileCompare(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::TextFileCompare)
    , comparedFilename{"compared.html"}
    , TIMER_TICK{500}
{
    ui->setupUi(this);
    setupTimer();

    file1_name = "";
    file2_name = "";
    compareToFirstFile = true;
}

TextFileCompare::~TextFileCompare()
{
    delete ui;
    delete timer;
}

void TextFileCompare::selectFilename(std::string &fname)
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setNameFilter(tr("Text Cpp (*.txt *.cpp *.c *.h *.hpp)"));
    dialog.setViewMode(QFileDialog::Detail);
    QStringList fileNames;
    if (dialog.exec())
        fileNames = dialog.selectedFiles();
    if (fileNames.size())
    {
        fname = fileNames.begin()->toStdString();
    }
}

void TextFileCompare::setupTimer()
{
    timer = new QTimer();
    timer->setInterval(std::chrono::milliseconds(TIMER_TICK) );
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
}

void TextFileCompare::enableControls()
{
    ui->selectFile1Button->setEnabled(true);
    ui->selectFile2Button->setEnabled(true);
    ui->compareButton->setEnabled(true);
}

void TextFileCompare::disableControls()
{
    ui->selectFile1Button->setEnabled(false);
    ui->selectFile2Button->setEnabled(false);
    ui->compareButton->setEnabled(false);
}

void TextFileCompare::on_selectFile1Button_clicked()
{
    selectFilename(file1_name);
    ui->lineEditFile1->setText(QString(file1_name.c_str()));
}

void TextFileCompare::on_selectFile2Button_clicked()
{
    selectFilename(file2_name);
    ui->lineEditFile2->setText(QString(file2_name.c_str()));
}

void TextFileCompare::updateTime()
{
    if (future_result.isFinished())
    {
        timer->stop();
        ui->textBrowser->clear();
        ui->textBrowser->setSource(QUrl(comparedFilename.c_str()));
        enableControls();
    }
}

void TextFileCompare::on_compareButton_clicked()
{
    disableControls();
    timer->start();

    const std::string loading_str =  "<font size = 16>Comparing ...</font>";
    ui->textBrowser->setText(loading_str.c_str());

    auto worker = [&]()
    {
        Comparer cmp;
        Renderer rndr(cmp.getColorText());
        TextFile f1, f2;

        f1.loadFromFile(file1_name);
        f2.loadFromFile(file2_name);

        cmp.compareFiles(f1, f2);
        rndr.savePrettyTextToHTML(comparedFilename, compareToFirstFile);
        return true;
    };
    future_result = QtConcurrent::run(worker);
}

void TextFileCompare::on_radioButton1_toggled(bool checked)
{
    compareToFirstFile = checked;
}
