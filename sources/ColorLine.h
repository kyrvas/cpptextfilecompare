#ifndef COLORLINE_H
#define COLORLINE_H

#include <vector>
#include <string>

class ColorLine
{
public:
	enum class Color {MATCH, COLOR_FILE1, COLOR_FILE2};
	std::vector<std::pair<std::string, Color> > line;
};

#endif