#ifndef COMPARER_H
#define COMPARER_H

#include "TextFile.h"
#include "ColorText.h"
#include <string>
#include <tuple>

class Comparer
{
public:
	void compareFiles(const TextFile& f1, const TextFile& f2);
	const ColorText& getColorText();
private:
	ColorText colored_txt;
	std::tuple<size_t, size_t, std::string> maxStringOf(const std::string& s1, const std::string& s2);
	void recursiveComparedColorLine(const std::string& s1, const std::string& s2, ColorLine& res);
};

#endif