#include "TextFile.h"
#include <fstream>

void TextFile::loadFromFile(const std::string& fname_)
{
	std::ifstream f;
	f.open(fname_);
	if (f.is_open())
	{
		std::string s;
		lines.clear();
		while(std::getline(f, s))
		{
			lines.push_back(s);
		}
	f.close();
	}
}