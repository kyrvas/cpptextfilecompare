#include "Renderer.h"
#include <iostream>
#include <map>
#include <fstream>

Renderer::Renderer(const ColorText& clrtxt): text(clrtxt)
{
}

void Renderer::drawPrettyTextToCout(bool comparedTofile1)
{
    std::map<ColorLine::Color, std::string> mod = { {ColorLine::Color::MATCH, "" }, 
                                                    {ColorLine::Color::COLOR_FILE1, "[add]" }, 
                                                    {ColorLine::Color::COLOR_FILE2, "[del]" },
    };
    
    if (!comparedTofile1)
        mod = { {ColorLine::Color::MATCH, "" },
                {ColorLine::Color::COLOR_FILE1, "[del]" },
                {ColorLine::Color::COLOR_FILE2, "[add]" },
    };

    
    for (const auto& lines : text.line_text_color)
    {
        for (const auto& string_color : lines.line)
        {
            std::cout << mod[string_color.second] << string_color.first << mod[string_color.second];
        }// line
        std::cout << "\n";
    }
}

void Renderer::savePrettyTextToHTML(const std::string& fname_, bool comparedTofile1)
{
    std::map<ColorLine::Color, std::string> mod = { {ColorLine::Color::MATCH, "<font color = ""black"">" },
                                                {ColorLine::Color::COLOR_FILE1, "<font color = ""green"">" },
                                                {ColorLine::Color::COLOR_FILE2, "<font color = ""red"">" },
    };

    if (!comparedTofile1)
        mod = { {ColorLine::Color::MATCH, "<font color = ""black"">" },
                {ColorLine::Color::COLOR_FILE1, "<font color = ""red"">" },
                {ColorLine::Color::COLOR_FILE2, "<font color = ""green"">" },
    };

    std::string mod_close = "</font>";
    std::string header_ ="<!DOCTYPE html>\n<html>\n<body>\n";
    std::string footer_ = "</body>\n</html>";

    std::ofstream f;
    f.open(fname_);
    if (f.is_open())
    {
        f << header_;
        for (const auto& lines : text.line_text_color)
        {
            f << "<p>";
            for (const auto& string_color : lines.line)
            {
                f << mod[string_color.second] << string_color.first << mod_close;
            }// line
            f << "</p>"<< "\n";
        }
        f << footer_;
        f.close();
    }
}