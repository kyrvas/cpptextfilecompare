#ifndef RENDERER_H
#define RENDERER_H

#include "ColorText.h"

class Renderer
{
public:
	Renderer(const ColorText& clrtxt);
	void drawPrettyTextToCout(bool comparedTofile1 = true);
	void savePrettyTextToHTML(const std::string& fname_ = "3.html", bool comparedTofile1 = true);

private:
	const ColorText& text;
};
#endif