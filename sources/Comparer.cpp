#include "Comparer.h"
#include "ColorLine.h"
#include <string>
#include <tuple>
#include <iostream>

std::tuple<size_t, size_t, std::string> Comparer::maxStringOf(const std::string& s1, const std::string& s2)
{
    const int min_compared = 1;
    std::string tmp, big_str, small_str;
    std::size_t res;
    bool normal_order = true;
    size_t l_big, l_small;
    if (s1.size() > s2.size())
    {
        big_str.assign(s1);
        small_str.assign(s2);
    }
    else
    {
        big_str.assign(s2);
        small_str.assign(s1);
        normal_order = false;
    }
    l_big = big_str.size();
    l_small = small_str.size();
    if (l_small == 0)
        return std::make_tuple(std::string::npos, std::string::npos, "");

    for (size_t i = 0; i < l_small - min_compared + 1; ++i)
    {
        for (size_t pos = 0; pos <= i; ++pos)
        {
            tmp.assign(small_str, pos, l_small - i);
            res = big_str.find(tmp);
            if (res != std::string::npos)
            {
                return (normal_order ? std::make_tuple(res, pos, tmp) : std::make_tuple(pos, res, tmp));
            }
        }
    }
    return std::make_tuple(std::string::npos, std::string::npos, "");
}

void Comparer::recursiveComparedColorLine(const std::string& s1, const std::string& s2, ColorLine& res)
{
    auto a = maxStringOf(s1, s2);

    const auto& match_str = std::get<2>(a);
    const auto& x1 = std::get<0>(a);
    const auto& x2 = std::get<1>(a);

    if (match_str != "")
    {
        auto sz = match_str.size();

        auto sz1 = s1.size();
        auto sz2 = s2.size();

        std::string lss1 = "", lss2 = "", rss1 = "", rss2 = "";

        lss1 = s1.substr(0, x1);
        lss2 = s2.substr(0, x2);

        rss1 = s1.substr(x1 + sz, sz1 - x1 - sz);
        rss2 = s2.substr(x2 + sz, sz2 - x2 - sz);

        if ((lss1 != "") && (lss2 != "")) // left
        {
            recursiveComparedColorLine(lss1, lss2, res);
        }
        else if ((lss1 == "") && (lss2 != ""))
        {
            res.line.push_back(std::make_pair(lss2, ColorLine::Color::COLOR_FILE2));
        }
        else if ((lss1 != "") && (lss2 == ""))
        {
            res.line.push_back(std::make_pair(lss1, ColorLine::Color::COLOR_FILE1));
        }

        res.line.push_back(std::make_pair(match_str, ColorLine::Color::MATCH));

        if ((rss1 != "") && (rss2 != "")) //right
        {
            recursiveComparedColorLine(rss1, rss2, res);
        }
        else if ((rss1 == "") && (rss2 != ""))
        {
            res.line.push_back(std::make_pair(rss2, ColorLine::Color::COLOR_FILE2));
        }
        else if ((rss1 != "") && (rss2 == ""))
        {
            res.line.push_back(std::make_pair(rss1, ColorLine::Color::COLOR_FILE1));
        }
    }
    else //== ""
    {
        res.line.push_back(std::make_pair(s1, ColorLine::Color::COLOR_FILE1));
        res.line.push_back(std::make_pair(s2, ColorLine::Color::COLOR_FILE2));
    }
}

void Comparer::compareFiles(const TextFile& f1, const TextFile& f2)
{
    auto sz1 = f1.lines.size();
    auto sz2 = f2.lines.size();
    std::size_t min_len = sz1 > sz2 ? sz2 : sz1;

    for (std::size_t i = 0; i < min_len; ++i)
    {
        colored_txt.line_text_color.push_back(ColorLine());
        recursiveComparedColorLine(f1.lines[i], f2.lines[i], colored_txt.line_text_color[i]);
    }

    if (sz1 > sz2)
    {
        for (std::size_t i = min_len; i< sz1;++i)
        {
            ColorLine cl;
            cl.line.push_back(std::make_pair(f1.lines[i], ColorLine::Color::COLOR_FILE1));
            colored_txt.line_text_color.push_back(cl);
        }
    }

    if (sz1 < sz2)
    {
        for (std::size_t i = min_len; i < sz2; ++i)
        {
            ColorLine cl;
            cl.line.push_back(std::make_pair(f2.lines[i], ColorLine::Color::COLOR_FILE2));
            colored_txt.line_text_color.push_back(cl);
        }
    }
}

const ColorText& Comparer::getColorText()
{
    return colored_txt;
}
