#ifndef TEXTFILE_H
#define TEXTFILE_H

#include <string>
#include <vector>

class TextFile
{
public:
	void loadFromFile(const std::string& fname_);
	std::vector<std::string> lines;
};
#endif